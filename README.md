# LiveviewGithub
Phoenix Liveview Component for streaming github repositories in realtime.
This is an example to show the capabilities of liveview as an alternative to SPA frameworks.

## Setting up the Application
  * start the terminal
  * Clone this application to current folder and extract
  * change directory to `liveview_github` folder

### Run the application

  * Setup the project with `mix setup`
  * Start Phoenix endpoint with `mix phx.server`
  * open `http:localhost:4000/` in the browser